using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using D08Jagadeesh.Models;

namespace D08Jagadeesh.Controllers
{
    [Produces("application/json")]
    [Route("api/Sections")]
    public class SectionsController : Controller
    {
        private AppDbContext _context;

        public SectionsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Sections
        [HttpGet]
        public IEnumerable<Section> GetSections()
        {
            return _context.Sections;
        }

        // GET: api/Sections/5
        [HttpGet("{id}", Name = "GetSection")]
        public IActionResult GetSection([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Section section = _context.Sections.Single(m => m.SectionId == id);

            if (section == null)
            {
                return HttpNotFound();
            }

            return Ok(section);
        }

        // PUT: api/Sections/5
        [HttpPut("{id}")]
        public IActionResult PutSection(int id, [FromBody] Section section)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != section.SectionId)
            {
                return HttpBadRequest();
            }

            _context.Entry(section).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SectionExists(id))
                {
                    return HttpNotFound();
                }
                else
                {
                    throw;
                }
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/Sections
        [HttpPost]
        public IActionResult PostSection([FromBody] Section section)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            _context.Sections.Add(section);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (SectionExists(section.SectionId))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("GetSection", new { id = section.SectionId }, section);
        }

        // DELETE: api/Sections/5
        [HttpDelete("{id}")]
        public IActionResult DeleteSection(int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Section section = _context.Sections.Single(m => m.SectionId == id);
            if (section == null)
            {
                return HttpNotFound();
            }

            _context.Sections.Remove(section);
            _context.SaveChanges();

            return Ok(section);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SectionExists(int id)
        {
            return _context.Sections.Count(e => e.SectionId == id) > 0;
        }
    }
}