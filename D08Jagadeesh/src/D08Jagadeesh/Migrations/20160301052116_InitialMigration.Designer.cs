using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using D08Jagadeesh.Models;

namespace D08Jagadeesh.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20160301052116_InitialMigration")]
    partial class InitialMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("D08Jagadeesh.Models.Location", b =>
                {
                    b.Property<int>("LocationID");

                    b.Property<string>("Country");

                    b.Property<string>("County");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("Place");

                    b.Property<int?>("SectionSectionId");

                    b.Property<string>("State");

                    b.Property<string>("StateAbbreviation");

                    b.Property<string>("ZipCode");

                    b.HasKey("LocationID");
                });

            modelBuilder.Entity("D08Jagadeesh.Models.Section", b =>
                {
                    b.Property<int>("SectionId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("FacultyId");

                    b.Property<int?>("LocationID");

                    b.Property<int>("capacity");

                    b.Property<int>("columns");

                    b.Property<int>("rows");

                    b.HasKey("SectionId");
                });

            modelBuilder.Entity("D08Jagadeesh.Models.Location", b =>
                {
                    b.HasOne("D08Jagadeesh.Models.Section")
                        .WithMany()
                        .HasForeignKey("SectionSectionId");
                });

            modelBuilder.Entity("D08Jagadeesh.Models.Section", b =>
                {
                    b.HasOne("D08Jagadeesh.Models.Location")
                        .WithMany()
                        .HasForeignKey("LocationID");
                });
        }
    }
}
